#!/bin/bash

X=$(cat /root/.bitgreen/bitgreen.conf | grep masternodeblsprivkey=)
## If no BLS key build one first!
if [ ! ${X} ]; then
    ./bitgreen-cli -blsgenerate >/root/.bitgreen/bls.json
    KEY=$(cat /root/.bitgreen/bls.json | grep secret | sed 's/\( "secret": "\)\|"\|,\| //g')
    echo "masternode=1" >>/root/.bitgreen/bitgreen.conf
    echo "masternodeblsprivkey=${KEY}" >>/root/.bitgreen/bitgreen.conf
fi

exec /usr/bin/supervisord "$@"

# Wait bitgreend process termination
PID=$(cat /root/.bitgreen/bitgreend.pid)
while [ -e /proc/$PID ]; do sleep .6; done
