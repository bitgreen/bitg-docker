#!/bin/bash

echo "empty syslog to claim space..."
rm -r /var/log/syslog.*
> /var/log/syslog

# Stop all images
echo "Stopping  ..."
systemctl stop docker
bitg_control.sh stop
cat <<EOF >/etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {"max-size": "10m", "max-file": "3"}
}
EOF
systemctl restart docker
echo "Prune docker ..."
docker system prune -f

echo "Repair nodes..."
command=$1
id=1
idstring=$(printf '%03d' ${id})
while [ -f "/etc/systemd/system/bitg-${idstring}.service" ]; do
  echo "bitg-${idstring}:"
  /opt/bitg/bitg_repair.sh "${id}"
  id=$((id + 1))
  idstring=$(printf '%03d' ${id})
done

