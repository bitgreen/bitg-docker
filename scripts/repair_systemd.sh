#!/bin/bash

id=1
idstring=$(printf '%03d' ${id})
while [ -f "/etc/systemd/system/bitg-${idstring}.service" ]; do
  sed -i '/^ExecStartPre=-\/usr\/bin\/docker rm/d' /etc/systemd/system/bitg-${idstring}.service
  sed -i 's/\/opt\/app\/bitgreen-cli stop/supervisorctl shutdown/g' /etc/systemd/system/bitg-${idstring}.service
  sed -i 's/docker exec/docker exec -i/g' /etc/systemd/system/bitg-${idstring}.service

  id=$((id + 1))
  idstring=$(printf '%03d' ${id})
done
systemctl daemon-reload
