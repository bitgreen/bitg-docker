#!/bin/bash

command=$1
id=1
idstring=$(printf '%03d' ${id})
while [ -f "/etc/systemd/system/bitg-${idstring}.service" ]; do
  echo "bitg-${idstring}:"
  systemctl $1 bitg-${idstring}.service
  if [ "$command" = "stop" ]; then
    docker stop bitg-${idstring} >/dev/null 2> /dev/null
    docker kill bitg-${idstring} >/dev/null 2> /dev/null
    docker rm bitg-${idstring} >/dev/null 2> /dev/null
  fi

  id=$((id + 1))
  idstring=$(printf '%03d' ${id})
done
