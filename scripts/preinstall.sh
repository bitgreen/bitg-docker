echo "####### Upgrading machine versions"
apt update && apt upgrade -y >/dev/null 2>&1
echo "####### installinging additional dependencies and docker if needed"
if ! apt-get install -y docker.io apt-transport-https curl fail2ban unattended-upgrades ufw dnsutils jq >/dev/null; then
  echo "Install cannot be completed successfully see errors above!"
fi

cat <<EOF >/etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {"max-size": "10m", "max-file": "3"}
}
EOF

systemctl restart docker

# Create swapfile if less then 2GB memory
totalmem=$(free -m | awk '/^Mem:/{print $2}')
totalswp=$(free -m | awk '/^Swap:/{print $2}')
totalm=$(($totalmem + $totalswp))
if [ $totalm -lt 4000 ]; then
  echo "Server memory is less then 2GB..."
  if ! grep -q '/swapfile' /etc/fstab; then
    echo "Creating a 2GB swapfile..."
    fallocate -l 2G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo '/swapfile none swap sw 0 0' >>/etc/fstab
  fi
fi

#####################
echo "####### Creating the docker mount directories..."
mkdir -p /mnt/bitg/ /opt/bitg/

echo "####### Adding bitg control directories to path"
if [[ $(cat ~/.bashrc | grep bitg | wc -l) -eq 0 ]]; then
  echo 'export PATH=$PATH:/opt/bitg' >>~/.bashrc
fi
source ~/.bashrc

docker login registry.gitlab.com -u bitg-pub -p fzxLG9DGzhznyWxkJ6oB >/dev/null 2>&1

## Download the real scripts here
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/install_bitg.sh -O /opt/bitg/install_bitg.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/multi_install_bitg.sh -O /opt/bitg/multi_install_bitg.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_control.sh -O /opt/bitg/bitg_control.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_all_params.sh -O /opt/bitg/bitg_all_params.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/uninstall_bitg.sh -O /opt/bitg/uninstall_bitg.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_mn_status.sh -O /opt/bitg/bitg_mn_status.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg-cli.sh -O /opt/bitg/bitg-cli.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_repair.sh -O /opt/bitg/bitg_repair.sh
wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_repair_all.sh -O /opt/bitg/bitg_repair_all.sh
chmod +x /opt/bitg/*.sh

echo
echo "####### SERVER INSTALLED COPY AND PASTE THE FOLLOWING COMMAND TO INSTALL YOUR FIRST NODE"
echo "source ~/.bashrc && install_bitg.sh"
