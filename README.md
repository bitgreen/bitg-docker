# bitg-docker

Full guide: 
https://medium.com/bitgreen/masternode-setup-guide-how-to-deploy-a-masternode-80a974cb447a

### Preinstall
you first need to install docker and a few needed dependencies. 
    
    bash <(wget -qO- -o- https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/preinstall.sh)
    
This will download the preinstall script and a few more scripts and add them to the PATH. After the install here is finished:

    reboot
    
### Install Nodes

To install only a single node login to your VPS after you rebooted it from the step before:

    install_bitg.sh
    
This will show you all needed information at the end and might take a minute.  To install multiple (i.E. 3) at once:

    multi_install_bitg.sh 3
    
Nodes will be enumerated starting with 001.

### Uninstall a node

If you want to uninstall a specific node:

    uninstall_bitg.sh 001
    
Will uninstall node 001 exchange parameter acordingly.

### bitgreen-cli

The CLI is wrapped to the specific nodes. Use:

    bitgreen-cli-001
    
If you want info for all at once there is a wrapper script iterating over all nodes:
    
    bitg-cli.sh 
    
Masternode status use one of the following:

    bitgreen-cli-001 masternode status
    bitg_mn_status.sh
    bitg_mn_status.sh | grep -E '^bitg|state'


### Start/Stop/Restart logs

Single nodes:
    
    systemctl start/stop/restart bitg-001

All all nodes on a Server:

    bitg_control.sh restart
    
logs:

    journalctl -u bitg-001
    
rolling logs:

    journalctl -fu bitg-001

### Repair in case of looped errors:
This will resync a node you want to repair.

    bitg_repair.sh 001
    
### Update a node to the newest version:

Future updates are easy. Just restart the service and you are done!

    bitg_control.sh restart
    
### Show all parameters (again)
For a single node:

    chainparams-001.sh 
    
All at once:
    
    bitg_all_params.sh 
    
### Config location

Enumerated per server exchange `001` accordingly

    /mnt/bitg/001/
    
### Update Script Versions:

In case anything does not work here copy and paste the following to your console to update all script versions:

    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/install_bitg.sh -O /opt/bitg/install_bitg.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/multi_install_bitg.sh -O /opt/bitg/multi_install_bitg.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_control.sh -O /opt/bitg/bitg_control.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_all_params.sh -O /opt/bitg/bitg_all_params.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/uninstall_bitg.sh -O /opt/bitg/uninstall_bitg.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_mn_status.sh -O /opt/bitg/bitg_mn_status.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg-cli.sh -O /opt/bitg/bitg-cli.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_repair.sh -O /opt/bitg/bitg_repair.sh
    wget https://gitlab.com/bitgreen/bitg-docker/raw/master/scripts/bitg_repair_all.sh -O /opt/bitg/bitg_repair_all.sh
    chmod +x /opt/bitg/*.sh